<?php
    $tz = 'HaNoi/VietNam';
    $timestamp = time();
    $dt = new DateTime("Bay gio la: ", new DateTimeZone($tz));
    $dt->setTimestamp($timestamp); 
    echo $dt->format('H:i d/m/Y');
?>
echo '<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            background-color: whitesmoke;
            max-width: 1170px;
        }

        .day02 {
            text-align: center;
            vertical-align: middle;
        }
        
        button {
            color: white;
            background-color: rgb(80, 80, 204);
            width: 200px;
            height: 40px;
        }

        input {
            width: 200px;
            height: 40px;
            margin-left: 40px;
        }

        .login {
            display: flex;
            align-items: center;
        }

        .password {
            display: flex;
            margin-top: 20px;
            align-items: center;
        }

        .enter {
            display: flex;
            margin-top: 20px;
            align-items: center;
        }
    </style>
    <div class="day02">
        <div class="login">
            <button>
                Tên đăng nhập
            </button>
            <input type="text" id="name_login">
        </div>
        <div class="password">
            <button>
                Mật khẩu
            </button>
            <input type="text" id="name_password">  
        </div>
        <div class="enter">
            <button>Đăng nhập</button>
        </div>
    </div> 
</body>
</html>';